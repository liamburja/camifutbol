onload=function(){

  formulario.onsubmit=function(e){

    var comprueba="";
    var ret=true;
    // VALIDAR NOMBRE
    var introduceNombre=document.getElementsByName("nombre")[0].value;
    if (introduceNombre == null || introduceNombre.length == 0 || !(/^\S+$/.test(introduceNombre))){
         nombre.style.outline="red solid 1px";
         comprueba += "El nombre introducido es incorrecto <br/>";
         ret=false;
    }else{
      nombre.style.outline="blue solid 1px";

    }
    // VALIDAR APELLIDO
    var introduceApellido = document.getElementsByName('apellidos')[0].value;
    if (introduceApellido == null || introduceApellido.length == 0 || !(/^\S+[\s+\S]*$/.test(introduceApellido))){
           apellidos.style.outline="red solid 1px";
         comprueba += "El apellido introducido es incorrecto <br/>";
         ret=false;
    }else{
      apellidos.style.outline="blue solid 1px";

    }
    // nombre y apellido no coincide
    if(introduceNombre===introduceApellido){
      comprueba+="El nombre y apellido no pueden ser iguales <br/>";
      ret=false;
    }
    //validar Email
    var mail = document.getElementsByName("Email")[0].value;
    if ( !/^\w+@\w+\.\w+$/.test(mail) ){
      Email.style.outline="red solid 1px";
      comprueba += "El email es incorrecto <br/>";
      ret = false;
    }else{
    Email.style.outline="blue solid 1px";

  }
    // validar telefono
    var tel = document.getElementsByName("telefono")[0].value;
    if (isNaN(tel) || !(/^\d{9,10}$/.test(tel))){
       telefono.style.outline="red solid 1px";
       comprueba += "El teléfono es incorrecto <br/>";
       ret = false;
     }else{
     telefono.style.outline="blue solid 1px";

   }

    // validar mensaje
    var introduceMensaje = document.getElementsByName("mensaje")[0].value;
    if (introduceMensaje == null || introduceMensaje.length == 0 || !(/^\S+[\s+\S]*$/.test(introduceMensaje))){

         comprueba += "El mensaje introducido es incorrecto <br/>";
         ret=false;
    }


    if(ret){
      e.preventDefault();
      comprueba = "Tu petición se ha enviado satisfactoriamente";
      resultado.style.color="green";
    }else{

      resultado.style.color="red";
    }
    resultado.innerHTML = comprueba;
    return ret;
  }


}
